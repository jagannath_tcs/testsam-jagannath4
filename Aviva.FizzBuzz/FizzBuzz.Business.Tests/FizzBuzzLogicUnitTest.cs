﻿namespace FizzBuzz.Business.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using NUnit.Framework;
    using FluentAssertions;
    using Resources;

    [TestClass]
    public class FizzBuzzLogicUnitTest
    {
        private FizzBuzzLogic fizzBuzzLogic;

        [SetUp]
        public void TestInitialize()
        {
            this.fizzBuzzLogic = new FizzBuzzLogic();
        }

        [TestCase(15, true)]
        [TestCase(14, false)]
        public void IsNumberDivisibleMethodReturnsValidFlagForGivenNumber(int inputFizzBuzzNumber, bool expectedResult)
        {
            var actualResult = this.fizzBuzzLogic.IsNumberDivisible(inputFizzBuzzNumber);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }

        [TestCase(false, "Fizz Buzz")]
        [TestCase(true, "Wizz Wuzz")]
        public void GetDisplayMessageMethodReturnsFizzBuzzOrWizzWuzzForGivenInput(bool isTodayWednesday, string expectedResult)
        {
            var actualResult = this.fizzBuzzLogic.GetDisplayMessage(isTodayWednesday);

            actualResult.ShouldBeEquivalentTo(expectedResult);
        }
    }
}
