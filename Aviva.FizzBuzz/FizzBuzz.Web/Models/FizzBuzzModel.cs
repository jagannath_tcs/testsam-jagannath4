﻿namespace FizzBuzz.Web.Models
{
    using System.ComponentModel.DataAnnotations;
    using Business.Resources;
    using System.Collections.Generic;

    public class FizzBuzzModel
    {
        [Required(ErrorMessageResourceType = typeof(FizzBuzzResource), ErrorMessageResourceName = "InputErrorMessage")]
        [Range(1, 1000, ErrorMessageResourceType = typeof(FizzBuzzResource), ErrorMessageResourceName = "RangeErrorMessage")]
        public int InputValue { get; set; }

        public IEnumerable<string> FizzBuzzValuesList { get; set; }

        public bool ShowNextLink { get; set; }

        public bool ShowPrevLink { get; set; }

        public int PageNumber { get; set; }
    }
}