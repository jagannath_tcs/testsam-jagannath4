﻿namespace FizzBuzz.Web.Controllers
{
    using System.Web.Mvc;
    using Business;
    using Models;
    using System.Linq;
    using System.Collections.Generic;
    using Business.Resources;

    public class FizzBuzzController : Controller
    {
        private readonly IFizzBuzzManager fizzBuzzManagerLogic;
        private const int ItemsPerPage = 20;

        public FizzBuzzController(IFizzBuzzManager fizzBuzzManagerLogic)
        {
            this.fizzBuzzManagerLogic = fizzBuzzManagerLogic;
        }

        [HttpGet]
        public ActionResult FizzBuzz()
        {
            return View("FizzBuzzView");
        }

        [HttpPost]
        public ActionResult FizzBuzz(FizzBuzzModel fizzBuzzModel)
        {
            var fizzBuzzList = this.fizzBuzzManagerLogic.GetFizzBuzzList(fizzBuzzModel.InputValue);
            fizzBuzzModel.FizzBuzzValuesList = GetDisplayValues(fizzBuzzList, 1, ItemsPerPage);
            fizzBuzzModel.PageNumber = 1;
            fizzBuzzModel.ShowNextLink = IsNextLinkToBeDisplayed(fizzBuzzModel, fizzBuzzList);
            return View("FizzBuzzView", fizzBuzzModel);
        }

        public ActionResult DisplayPaging(int inputValue, int pageNumber, string linkText)
        {
            var fizzBuzzModel = new FizzBuzzModel
            {
                InputValue = inputValue,
                PageNumber = pageNumber
            };
            var fizzBuzzList = this.fizzBuzzManagerLogic.GetFizzBuzzList(fizzBuzzModel.InputValue);

            if (linkText == FizzBuzzResource.Next)
            {
                fizzBuzzModel.PageNumber = pageNumber + 1;
            }
            else if (linkText == FizzBuzzResource.Prev)
            {
                fizzBuzzModel.PageNumber = pageNumber - 1;
            }

            fizzBuzzModel.ShowPrevLink = fizzBuzzModel.PageNumber > 1;
            fizzBuzzModel.ShowNextLink = IsNextLinkToBeDisplayed(fizzBuzzModel, fizzBuzzList);
            fizzBuzzModel.FizzBuzzValuesList = GetDisplayValues(fizzBuzzList, fizzBuzzModel.PageNumber, ItemsPerPage);

            return View("FizzBuzzView", fizzBuzzModel);
        }

        private IEnumerable<string> GetDisplayValues(IEnumerable<string> fizzBuzzList, int pageNumber, int itemsShownPerPage)
        {
            var skippedItemsLogic = (pageNumber - 1)*itemsShownPerPage;
            return fizzBuzzList == null ? Enumerable.Empty<string>() : fizzBuzzList.Skip(skippedItemsLogic).Take(itemsShownPerPage);
        }

        private bool IsNextLinkToBeDisplayed(FizzBuzzModel fizzBuzzModel, IEnumerable<string> fizzBuzzList)
        {
            var isNextLinkDisplayed = false;
            var count = fizzBuzzList.Count();
            if (fizzBuzzList != null)
            {
                isNextLinkDisplayed = fizzBuzzModel.PageNumber <
                       ((count / ItemsPerPage) +
                        (count % ItemsPerPage == 0 ? 0 : 1));
            }
            return isNextLinkDisplayed;
        }
    }
}
