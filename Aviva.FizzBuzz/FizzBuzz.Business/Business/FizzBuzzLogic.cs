﻿namespace FizzBuzz.Business
{
    using Resources;

    public class FizzBuzzLogic : IDivisionPolicy
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return (inputFizzBuzzNumber % 15) == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return isTodayWednesday ? FizzBuzzResource.WizzWuzz : FizzBuzzResource.FizzBuzz;
        }

        public int Order { get; set; }
    }
}
