﻿namespace FizzBuzz.Business
{
    using Resources;

    public class FizzLogic : IDivisionPolicy
    {
        public bool IsNumberDivisible(int inputFizzBuzzNumber)
        {
            return inputFizzBuzzNumber % 3 == 0;
        }

        public string GetDisplayMessage(bool isTodayWednesday)
        {
            return isTodayWednesday ? FizzBuzzResource.Wizz : FizzBuzzResource.Fizz;
        }

        public int Order { get; set; }
    }
}
